﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Discuz.Common
{
    public class AsteriskHelper
    {
        /// <summary>
        /// 获取带有星号的用户名
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string GetNameWithAsterisk(object obj)
        {
            string name = obj.ToString().Trim();
            int len = name.Length;
            if (len >= 3)
            {
                int count = len / 3;
                string start = name.Substring(0, count), end = string.Empty;
                int remainer = len % 3;
                if (remainer == 0)
                {
                    end = name.Substring(count * 2);
                }
                else
                {
                    end = name.Substring(count * 2 + remainer);
                }
                for (int i = 0; i < count + remainer; i++)
                {
                    start += "*";
                }
                name = start + end;
            }
            else if (len == 2)
            {
                name = name[0].ToString() + "*";
            }
            return name;
        }
        /// <summary>
        /// 修改版主显示名称加星号
        /// </summary>
        /// <param name="moderator"></param>
        /// <returns></returns>
        public static string GetModeratorsWithAsterisk(string moderator)
        {
            moderator = moderator.Trim();
            string source = moderator;
            string douhao = ",";
            if (!string.IsNullOrEmpty(moderator))
            {
                string[] list = moderator.Split(new String[] { douhao }, StringSplitOptions.RemoveEmptyEntries);
                string newModerator = string.Empty;
                foreach (var item in list)
                {
                    newModerator += GetNameWithAsterisk(item) + douhao;
                }
                moderator = newModerator.Remove(newModerator.LastIndexOf(douhao));
                return moderator.Trim();
            }
            return moderator;
        }
        /// <summary>
        /// 修改最后编辑人加星号
        /// </summary>
        /// <param name="lastEditor"></param>
        /// <returns></returns>
        public static string GetLastEditorWithAsterisk(string lastEditor)
        {
            lastEditor = lastEditor.Trim();
            if (!string.IsNullOrEmpty(lastEditor))
            {
                string[] list = lastEditor.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                string poster = list[0];
                if (!string.IsNullOrEmpty(poster))
                {
                    list[0] = GetNameWithAsterisk(poster);
                }
                lastEditor = string.Join(" ", list);
            }
            return lastEditor;
        }
    }
}
